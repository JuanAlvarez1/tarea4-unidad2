#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"
#Crear un programa en Python que utilice la librería pygame ytodo el código bujde la Tarea 3  para diar un plano cartesiano y representar gráficamente los objetos Punto y los objetos Rectángulo creados en la tarea anterior. La salida del programa debe ser capturada en una imagen (similar a la mostrada al final) y guardada en el mismo repositorio gitlab de esta tarea"
import pygame

import random
CYAN = (0, 255, 255)
AMARILLO = (255, 255, 0)
CAFE = (90, 50, 15)
VERDE = (10, 255, 10)
BLANCO = (255, 255, 255)
pygame.init()
Dimensiones = (900, 700)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Plano cartesiano")


Fuente = pygame.font.Font(None, 20)
Texto = Fuente.render("Representacion Grafica del Plano Cartesiano", True, BLANCO)
X= Fuente.render("X", True, BLANCO)
Y = Fuente.render("Y", True, BLANCO)
Cuadrado =Fuente.render("250 X 120", True, BLANCO)
Uno = Fuente.render("1", True, BLANCO)
Dos = Fuente.render("2", True, BLANCO)
Uno1 = Fuente.render("1", True, BLANCO)
Dos2 = Fuente.render("2", True, BLANCO)
Var = Fuente.render("250", True, BLANCO)
Var2 = Fuente.render("120", True, BLANCO)


Terminar = False
reloj = pygame.time.Clock()

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(CYAN)
     pygame.draw.line(Pantalla, BLANCO, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, BLANCO, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, AMARILLO, (300, 300, 250, 120), 0)
     Pantalla.blit(Texto, [10, 10])
     Pantalla.blit(X, [585, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 340])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [330, 280])
     Pantalla.blit(Var, [540, 280])
     Pantalla.blit(Var2, [275, 410])
     Pantalla.blit(Cuadrado, [400, 350])



     pygame.display.flip()
     reloj.tick(20)
pygame.quit()
